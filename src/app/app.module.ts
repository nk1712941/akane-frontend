import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule} from '@angular/material/toolbar';
import { SidebarComponent } from './component/sidebar/sidebar.component';
import { MatSidenavModule} from '@angular/material/sidenav';
import { MatListModule} from '@angular/material/list';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { TaskListComponent } from './component/task-list/task-list.component';
import { MatTableModule} from '@angular/material/table';
import { RouterModule} from '@angular/router';
import { AppRoutingModule} from './module/route/app-routing.module';
import { TaskLogComponent } from './component/task-log/task-log.component';
import { ExecutorListComponent } from './component/executor-list/executor-list.component';
import {MatSortModule} from '@angular/material/sort';
import {MatButtonModule} from '@angular/material/button';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {ConfirmDialogComponent} from './component/dialog/confirm-dialog/confirm-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { TaskDialogComponent } from './component/dialog/task-dialog/task-dialog.component';
import {MatMenuModule} from '@angular/material/menu';
import {MatSelectModule} from '@angular/material/select';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    DashboardComponent,
    TaskListComponent,
    TaskLogComponent,
    ExecutorListComponent,
    ConfirmDialogComponent,
    TaskDialogComponent
  ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        MatSidenavModule,
        MatListModule,
        MatTableModule,
        RouterModule,
        AppRoutingModule,
        HttpClientModule,
        MatSortModule,
        MatButtonModule,
        MatPaginatorModule,
        MatAutocompleteModule,
        ReactiveFormsModule,
        MatInputModule,
        MatDialogModule,
        MatSnackBarModule,
        MatMenuModule,
        FormsModule,
        MatSelectModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
