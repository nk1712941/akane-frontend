import { Component, OnInit } from '@angular/core';

import * as echarts from 'echarts';
import {LogService} from '../../service/log/log.service';
import {westerosData} from '../../../assets/plugins/echarts/westeros';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private logService: LogService) { }

  ngOnInit(): void {
    const dom1 = document.getElementById('chart1');
    if (dom1 == null) {
      return;
    }
    echarts.registerTheme('westeros', westerosData);
    const instance1 = echarts.init(dom1, 'westeros');
    this.logService.getChartData().subscribe(res => {
      const option = {
        title: {
          text: '任务报表'
        },
        grid: [
          {left: '10%', width: '70%', bottom: '10%', height: '70%'}
        ],
        tooltip: {
          trigger: 'axis',
          axisPointer: {
            type: 'cross',
            label: {
              backgroundColor: '#6a7985'
            }
          }
        },

        legend: {
        },
        dataset: {
          dimensions: ['date', '运行成功', '运行失败'],
          source: res.data.barData.map(data => [data.date, data.successCount, data.failureCount])
        },
        xAxis: {type: 'category'},
        yAxis: {},
        series: [
          {
          type: 'line',
          areaStyle: {
            },
            emphasis: {
            lineStyle: {
              width: '2'
            }
            }
          },
          {
            type: 'line',
            areaStyle: {
              },
            emphasis: {
              lineStyle: {
                width: '2'
              }
            }
          },
          {
            type: 'pie',
            radius: '75%',
            right: 'right',
            width: '35%',
            top: '10%',
            height: '35%',
            data: [{
              value: res.data.successCount, name: '运行成功'
            }, {
              value: res.data.failureCount, name: '运行失败'
            }, {
              value: res.data.runningCount, name: '运行中'
            }]
          }]
      };
      instance1.setOption(option);
    });
  }

}
