import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Task} from '../../../interface/task';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-task-dialog',
  templateUrl: './task-dialog.component.html',
  styleUrls: ['./task-dialog.component.css']
})
export class TaskDialogComponent implements OnInit {
  title: string;
  constructor(public dialogRef: MatDialogRef<TaskDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public task: Task) {
    this.title = task.id === undefined ? '新建任务' : '编辑任务';
  }

  ngOnInit(): void {
  }
  onNoClick(): void {
    this.dialogRef.close(null);
  }
  onOKClick(): void {
    this.dialogRef.close(this.task);
  }
}
