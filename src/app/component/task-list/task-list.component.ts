import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';

import { TaskService } from '../../service/task/task.service';
import {Task, TaskTemplate} from '../../interface/task';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {ConfirmDialogComponent} from '../dialog/confirm-dialog/confirm-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TaskDialogComponent} from '../dialog/task-dialog/task-dialog.component';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit, AfterViewInit {
  constructor(private taskService: TaskService, public dialog: MatDialog, private snackBar: MatSnackBar) { }
  columns = ['id', 'name', 'executorHandler', 'author', 'triggerStatus', 'action'];
  dataSource: MatTableDataSource<Task> = null;
  handlerList: string[] = [];
  filteredHandlerList: Observable<string[]> = null;
  control = new FormControl();
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dateChanger(date: string): string {
    return date.replace('T', ' ').replace('.000+00:00', '');
  }

  ngOnInit(): void {
    this.getTaskList();
    this.filteredHandlerList = this.control.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
    this.control.valueChanges.subscribe(value => this.dataSource.filter = value);
  }
  ngAfterViewInit(): void {
  }

  getTaskList(): void {
    this.taskService.getTaskList().subscribe(res => {
      res.data.forEach(task => {
        task.createTime = this.dateChanger(task.createTime);
        task.updateTime = this.dateChanger(task.updateTime);
        this.handlerList.push(task.executorHandler);
      });
      this.dataSource = new MatTableDataSource<Task>(res.data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.handlerList.filter(handler => handler.includes(filterValue));
  }
  runTask(id: number): void {
    this.taskService.runTask(id).subscribe(response => {
      this.snackBar.open(response.message, 'OK', {
        duration: 1000
      });
    });
  }
  deleteTask(id: number, appName: string): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {width: '400px', data: appName});
    dialogRef.afterClosed().subscribe(res => {
      if (res === 'OK') {
        this.taskService.deleteTask(id).subscribe(response => {
          this.snackBar.open(response.message, 'OK', {
            duration: 1000
          });
          this.getTaskList();
        });
      }
    });
  }
  addTask(): void {
    const task = new TaskTemplate();
    const dialogRef = this.dialog.open(TaskDialogComponent, {data: task});
    dialogRef.afterClosed().subscribe(res => {
      if (res != null) {
        this.taskService.addTask(res).subscribe(response => {
          this.snackBar.open(response.message, 'OK', {
            duration: 1000
          });
          this.getTaskList();
        });
      }
    });
  }
  editTask(task: Task): void {
    const dialogRef = this.dialog.open(TaskDialogComponent, {data: task});
    dialogRef.afterClosed().subscribe(res => {
      if (res != null) {
        this.taskService.updateTask(res).subscribe(response => {
          this.snackBar.open(response.message, 'OK', {
            duration: 1000
          });
          this.getTaskList();
        });
      }
    });
  }
  startTask(taskId: number): void {
    this.taskService.startTask(taskId).subscribe(res => {
      this.snackBar.open(res.message, 'OK', {
        duration: 1000
      });
      this.getTaskList();
    });
  }
  stopTask(taskId: number): void {
    this.taskService.stopTask(taskId).subscribe(res => {
      this.snackBar.open(res.message, 'OK', {
        duration: 1000
      });
      this.getTaskList();
    });
  }
  toLogPage(task: Task): void {
    window.open('log');
  }
}
