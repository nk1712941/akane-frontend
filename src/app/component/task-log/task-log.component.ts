import {Component, OnInit, ViewChild} from '@angular/core';
import {LogService} from '../../service/log/log.service';
import {MatTableDataSource} from '@angular/material/table';
import {Log} from '../../interface/log';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';


@Component({
  selector: 'app-task-log',
  templateUrl: './task-log.component.html',
  styleUrls: ['./task-log.component.css']
})
export class TaskLogComponent implements OnInit {
  columns = ['taskId', 'triggerTime', 'triggerCode', 'handleTime', 'handleCode', '操作'];
  dataSource: MatTableDataSource<Log>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private logService: LogService) { }

  ngOnInit(): void {
    this.getTaskLogList();
  }
  getTaskLogList(): void {
    this.logService.getTaskLogList().subscribe(res => {
      res.data.forEach(log => {
        log.triggerTime = this.dateChanger(log.triggerTime);
        log.handleTime = this.dateChanger(log.handleTime);
      });
      this.dataSource = new MatTableDataSource<Log>(res.data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }
  dateChanger(date: string): string {
    if (date === null) {
      return null;
    }
    return date.replace('T', ' ').replace('.000+00:00', '');
  }

}
