export interface ChartData {
  successCount: number;
  failureCount: number;
  runningCount: number;
  barData: BarData[];
}

export interface BarData {
  date: string;
  successCount: string;
  failureCount: string;
}
