export interface Group {
  id?: number;
  appName: string;
  addressList: string;
  createTime?: string;
  updateTime?: string;
  addressType: number;
}
export class GroupTemplate implements Group{
  appName: string;
  addressList: string;
  addressType: number;
  constructor() {
  }
}
