export interface Log {
  id: number;
  taskId: number;
  triggerTime: string;
  handleTime?: string;
  executorAddress: string;
  executorHandler: string;
  executorParam?: string;
  triggerCode: number;
  handleCode?: number;
  triggerMsg: string;
}
