export interface Task {
  id?: number;
  appName: string;
  executorHandler: string;
  updateTime?: string;
  createTime?: string;
  taskGroup: number;
  alarmEmail: string;
  author: string;
  triggerStatus: boolean;
  childId: string;
  executorParam: string;
  scheduleType: string;
  scheduleConf: string;
  misfireStrategy: string;
  retryCount: number;
  timeout: number;
  routeStrategy: string;
}

export class TaskTemplate implements Task{
  appName: string;
  executorHandler: string;
  taskGroup: number;
  alarmEmail: string;
  author: string;
  triggerStatus: boolean;
  childId: string;
  executorParam: string;
  scheduleType: string;
  scheduleConf: string;
  constructor() {
    this.triggerStatus = false;
  }
  misfireStrategy: string;
  retryCount: number;
  routeStrategy: string;
  timeout: number;
}
