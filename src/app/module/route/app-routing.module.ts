import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {TaskListComponent} from '../../component/task-list/task-list.component';
import {DashboardComponent} from '../../component/dashboard/dashboard.component';
import {TaskLogComponent} from '../../component/task-log/task-log.component';
import {ExecutorListComponent} from '../../component/executor-list/executor-list.component';

const routes: Routes = [
  {path: '', redirectTo: 'list', pathMatch: 'full'},
  {path: 'list', component: TaskListComponent },
  {path: 'dashboard', component: DashboardComponent},
  {path: 'log', component: TaskLogComponent},
  {path: 'executors', component: ExecutorListComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
