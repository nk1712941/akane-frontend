import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GroupService {
  baseUrl = 'http://localhost:8777/group/';
  constructor(httpClient: HttpClient) { }
}
