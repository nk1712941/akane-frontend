import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ServerResponse} from '../../interface/server-response';
import {Log} from '../../interface/log';
import {BarData, ChartData} from '../../interface/chart-data';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LogService {
  private baseUrl = 'http://localhost:8777/log';

  constructor(private http: HttpClient) { }
  getTaskLogList(): Observable<ServerResponse<Log[]>> {
    return this.http.get<ServerResponse<Log[]>>(this.baseUrl).pipe();
  }
  getChartData(): Observable<ServerResponse<ChartData>> {
    return this.http.get<ServerResponse<ChartData>>(this.baseUrl + '/chart')
      .pipe(tap(res => {
        console.log(res);
        res.data.barData.forEach(data => {
          data.date = data.date.replace(/ 00.*/, '');
        });
      }));
  }
}
