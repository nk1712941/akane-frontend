import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MessageService} from '../message/message.service';
import {Observable} from 'rxjs';
import {Task} from '../../interface/task';
import {ServerResponse} from '../../interface/server-response';
import {catchError, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private baseUrl = 'http://localhost:8777/task/';

  constructor(private messageService: MessageService, private http: HttpClient) { }
  log(message: string): void {
    this.messageService.add('TaskService: ${message}');
  }
  getTaskList(): Observable<ServerResponse<Task[]>> {
    return this.http.get<ServerResponse<Task[]>>(this.baseUrl + 'list')
      .pipe(
        tap(res => console.log(res))
      );
  }
  runTask(taskId: number): Observable<ServerResponse<any>> {
    console.log('run ' + taskId);
    return this.http.post<ServerResponse<any>>(this.baseUrl + 'run/' + taskId, null)
      .pipe(
        tap(res => console.log(res))
      );
  }
  deleteTask(taskId: number): Observable<ServerResponse<any>> {
    console.log('delete ' + taskId);
    return this.http.delete<ServerResponse<any>>(this.baseUrl + taskId)
      .pipe(
        tap(res => console.log(res))
    );
  }
  addTask(task: Task): Observable<ServerResponse<any>> {
    return this.http.put<ServerResponse<any>>(this.baseUrl, task)
      .pipe(
        tap(res => console.log(res))
      );
  }
  updateTask(task: Task): Observable<ServerResponse<any>> {
    return this.http.post<ServerResponse<any>>(this.baseUrl + 'update', task)
      .pipe(
        tap(res => console.log(res))
      );
  }
  startTask(id: number): Observable<ServerResponse<any>> {
    return this.http.post<ServerResponse<any>>(this.baseUrl + 'start/' + id, null)
      .pipe(
        tap(res => console.log(res))
      );
  }
  stopTask(id: number): Observable<ServerResponse<any>> {
    return this.http.post<ServerResponse<any>>(this.baseUrl + 'stop/' + id, null)
      .pipe(
        tap(res => console.log(res))
      );
  }
}
